package hr.fer.ruazosa.lecture8.demo1

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import hr.fer.ruazosa.lecture8.demo1.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var sleepService: SleepService
    private var bound: Boolean = false
    private lateinit var binding: ActivityMainBinding

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as SleepService.SleepServiceBinder
            sleepService = binder.service
            bound = true
            Toast.makeText(this@MainActivity, "Service connected", Toast.LENGTH_LONG).show()
        }
        override fun onServiceDisconnected(arg0: ComponentName) {
            bound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.startBoundServiceButton.setOnClickListener {
            lifecycleScope.launch {
                sleepService.sleep(5)
                launch(Dispatchers.Main) {
                    Toast.makeText(this@MainActivity,
                        "Wait method on sleep service finished", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        // Bind to LocalService
        Intent(this, SleepService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        unbindService(connection)
    }
}